/*
    Daily Appointments - an example for students to draw a UML Class diagram from.
    Copyright (C) 2017 Karl R. Wurst

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Iterator;
import java.util.TreeSet;

public class Day {
	
	private TreeSet<Event> events;
	
	public Day() {
		events = new TreeSet<Event>();
	}
	
	public void addEvent(Event event) {
		events.add(event);
	}
	
	public void removeEvent(Event event ) {
		events.remove(event);
	}
	
	public void displayDay() {
		Iterator<Event> i = events.iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}
	}

}
