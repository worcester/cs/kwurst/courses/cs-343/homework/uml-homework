/*
    Daily Appointments - an example for students to draw a UML Class diagram from.
    Copyright (C) 2017 Karl R. Wurst

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.time.LocalTime;

public class Appointment extends Event {
	
	private Person person;
	
	public Appointment(LocalTime startTime, LocalTime endTime, String description, Person person) {
		super(startTime, endTime, description);
		this.person = person;
	}

	@Override
	public String toString() {
		return "Appointment [person=" + person + ", toString()=" + super.toString() + "]";
	}

}
