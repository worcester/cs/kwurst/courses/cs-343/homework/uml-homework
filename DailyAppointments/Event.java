/*
    Daily Appointments - an example for students to draw a UML Class diagram from.
    Copyright (C) 2017 Karl R. Wurst

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.time.LocalTime;

public abstract class Event implements Comparable {
	private LocalTime startTime;
	private LocalTime endTime;
	private String description;
	
	public Event(LocalTime startTime, LocalTime endTime, String description) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.description = description;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Event [startTime=" + startTime + ", endTime=" + endTime + ", description=" + description + "]";
	}

	@Override
	public int compareTo(Object o) {
		if (o == null)
			return 1;
		if (getClass() != o.getClass())
			return 1;
		Event other = (Event)o;
		if (getStartTime().compareTo(other.getStartTime()) != 0) {
			return getStartTime().compareTo(other.getStartTime());
		} else {
			return getEndTime().compareTo(other.getEndTime());
		}
	}

}
