# Homework &mdash; Modeling with UML Class and Sequence Diagrams

## Objective

The objective of this assignment is for you to practice with UML class and sequence diagrams. You will practice creating a class diagram from existing code, and creating code from a sequence diagram.

All diagrams are written in [PlantUML](https://gitlab.com/worcester/cs/kwurst/software-development-supplemental-materials#plantuml). You must create new diagrams in PlantUML.

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### From Code to Class Diagram

For the [Daily Appointments](./DailyAppointments) code create a UML Class Diagram documenting the structure of the code. You are producing [UML as blueprint](https://www.martinfowler.com/bliki/UmlAsBlueprint.html). 

* You must use PlantUML to create your diagram.
* You must include all classes in your diagram
  * If a class is abstract or an interface, that must be shown with the correct formatting
  * Each class must include all properties (variables) with name, type, and visibility
    * If a property is abstract or static, that must be shown with the correct formatting
  * Each class must include all operations with name, parameters (with type), return type, and visibility
    * If an operation is abstract or static, that must be shown with the correct formatting
  * If the code imposes constraints on any properties or parameters, document them with a note.
* You must include all connections between classes using the appropriate arrow style, with the arrow head pointed in the correct direction. Be sure the multiplicity or property appears on the correct end of the arrow.
  * For associations, you must include any multiplicities other than 1.
  * For associations, you must label it with the property that makes the connection

> Insert the UML Class Diagram for the Base Assignment here.

## *Intermediate Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of C or higher**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### From Sequence Diagram to Code

From the sequence diagram below, write Java code for the calls to the [DailyAppointments](./DailyAppointments/) code from the user. (Think of this like a main method, but you do not need to put it in an actual method.)

Feel free to look at online sources for Java documentation for standard classes and syntax if you need a refresher.

```plantuml
hide footbox
autoactivate on

actor User

create participant sept182023
User -> sept182023: new
return
create participant sept192023
User -> sept192023: new
return

create participant cs34301
User -> cs34301: new(1530,1645,"CS-343-01","ST 107")
return
create participant cs34801
User -> cs34801: new(1400,1515,"CS-348-01","ST 107")
return
create participant sam
User -> sam: new("sam","508-123-5467","sam@worcester.edu") 
return
create participant indepStudy
User -> indepStudy: new
return


User -> sept182023: addEvent(cs34301)
return
User -> sept182023: addEvent(cs34801)
return
User -> sept182023: addEvent(indepStudy)
return

create participant cs34802
User -> cs34802: new (1300,1415,"CS-348-02","ST 107")
return

User -> sept192023: addEvent(cs34802)
return

User -> sept182023: displayDay()
return

User -> sept192023: displayDay()
return

User -> sept182023: removeEvent(indepStudy)
return

User -> sept182023: displayDay()
return

```

* Declare all the properties (variables) correctly.
* Make all method calls correctly **in the order shown in the sequence diagram**.
* Put your code in the space below:

```java
// put your code here
```

## *Advanced Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of B or higher**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Advanced Code to Class Diagram

For the [Order System](./OrderSystem) code create a UML Class Diagram documenting the structure of the code. You are producing [UML as blueprint](https://www.martinfowler.com/bliki/UmlAsBlueprint.html).

* You must use PlantUML to create your diagram.
* You must include all classes in your diagram
  * If a class is abstract or an interface, that must be shown with the correct formatting
  * Each class must include all properties (variables) with name, type, and visibility
    * If a property is abstract or static, that must be shown with the correct formatting
  * Each class must include all operations with name, parameters (with type), return type, and visibility
    * If an operation is abstract or static, that must be shown with the correct formatting
  * If the code imposes constraints on any properties or parameters, document them with a note.
* You must include all connections between classes using the appropriate arrow style, with the arrow head pointed in the correct direction. Be sure the multiplicity or property appears on the correct end of the arrow.
  * For associations, you must include any multiplicities other than 1
  * For associations, you must label it with the property that makes the connection

> Insert the UML Class Diagram for the Advanced Add-On here.

## Deliverables

* You must fork this Homework repository to your private GitLab subgroup of the CS 343 01 02 Fall 2023 group. It will look something like

`Worcester State University / Computer Science Department / CS 343 01 02 Fall 2024 / Students / username`

* You must create your UML Class Diagram for the Base Assignment in Markdown using PlantUML. It must be inserted into this document at the point identified in the `Base Assignment` section.
* If you choose to complete the Intermediate Add-On, you must write your code in the space provided at the end of the `Intermediate Add-on` section.
* If you choose to complete the Advanced Add-On, you must create your UML Class Diagram in Markdown using PlantUML. It must be inserted into this document at the point identified in the `Advanced Add-On` section.
* **Be sure that you add, commit, and push your changes to GitLab.**

&copy; 2024 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
