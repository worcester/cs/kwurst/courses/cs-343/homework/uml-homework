# Homework 1

**For the actual homework assignment, go to [HW1.md](./HW1.mdHW1.md).**

This assignment has a document with UML diagrams to implement in Java code, and Java code to represent with a UML diagram.

## Requires

* VS Code

## Working in Visual Studio Code

From the GitLab page for this project, do the following:

1. Fork this project to your `https://gitlab.com/worcester/cs/cs-343-01-fall-2023/students/your-name` subgroup.
2. (**In your fork**) Click on the `Clone` button.

    ![Clone button](images/clone.png)
3. Choose `Open in your IDE` - `Visual Studio Code`. (Use `SSH` or `HTTPS` depending on whether you have an SSH key or not.) ![Open in IDE](images/openinide.png)
4. If VSCode asks you "Do you want to install recommended extensions?", choose the option "Install".
5. If VSCode warns you that "Some content has been disabled in this document", click on the message and choose the option "Allow insecure content".

## Viewing the assignment in the PlantUML Previewer

1. Open [HW1.md](./HW1.md)
2. Open the preview ![Preview](images/preview.png) (upper right corner of the window).
3. Add your UML diagrams directly in `HW1.md` file using PlantUML.

## Submitting your assignment

**You should do your work in the `main` branch.** This is OK because you are not submitting this as a merge request.

1. `git add` your changes
2. `git commit` your changes
3. `git push` your changes

Copyright © 2023 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
